import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CanActivateRoute } from './shared/guards/can-activate-route.service';

const routes: Routes = [
  { path: '', redirectTo: '/todoslist', pathMatch: 'full' },
  { path: 'todoslist', loadChildren: './todos-list/todos-list.module#TodosListModule', canActivate: [CanActivateRoute] },
  { path: 'login', loadChildren: './app-login/app-login.module#AppLoginModule' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
