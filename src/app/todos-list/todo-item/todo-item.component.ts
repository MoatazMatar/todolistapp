import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { TodosService } from 'src/app/shared/services/todos.service';
import { UserService } from 'src/app/shared/services/user.service';
import { TodoItem, User } from 'src/app/shared/models/models';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.css']
})
export class TodoItemComponent implements OnInit {

  currentTodo: TodoItem;
  todosList: TodoItem[];
  currentTodoId: String;
  currentUser: User;
  newTodo: Boolean = false;
  TodoForm: FormGroup;

  // tslint:disable-next-line:max-line-length
  constructor(private route: ActivatedRoute, private router: Router, private todosService: TodosService, private userService: UserService, private fb: FormBuilder) { }

  ngOnInit() {
    this.todosList = this.todosService.getTodos();
    this.currentUser = this.userService.getCurrentUser();
    this.currentTodoId = this.route.snapshot.params['id'];
    if (this.currentTodoId === 'new') {
      this.newTodo = true;
      this.currentTodo = new TodoItem();
      this.currentTodo.id = this.todosService.getNewTodoId();
      this.currentTodo.createdById = this.currentUser.id;
    } else {
      this.currentTodo = this.todosService.getTodoByIndex(+this.currentTodoId);
    }
    this.initialTodoForm();
  }

  initialTodoForm() {
    this.TodoForm = this.fb.group({
      title: [this.newTodo ? '' : this.currentTodo.title, [Validators.required]],
      description: [this.newTodo ? '' : this.currentTodo.description, [Validators.required]],
      complete: [this.newTodo ? '' : this.currentTodo.complete],
    });
  }

  cancel() {
    this.router.navigate(['todoslist']);
  }

  saveTodoItem() {
    const todoFormData = this.TodoForm.value;
    this.currentTodo.title = todoFormData.title;
    this.currentTodo.description = todoFormData.description;
    this.currentTodo.complete = todoFormData.complete === '' ? false : todoFormData.complete;
    if (this.newTodo) {
      this.todosList = this.todosService.createItem(this.currentTodo);
    } else {
      this.todosList = this.todosService.updateItem(+this.currentTodoId, this.currentTodo);
    }
    this.router.navigate(['todoslist']);
  }

}
