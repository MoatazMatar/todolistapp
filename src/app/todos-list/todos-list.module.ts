import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TodosListRoutingModule } from './todos-list-routing.module';
import { TodosComponent } from './todos/todos.component';
import { TodosListComponent } from './todos-list/todos-list.component';
import { TodoItemComponent } from './todo-item/todo-item.component';

@NgModule({
  declarations: [TodosListComponent, TodoItemComponent, TodosComponent],
  imports: [
    CommonModule,
    TodosListRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class TodosListModule { }
