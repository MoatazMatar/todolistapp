import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UserService } from 'src/app/shared/services/user.service';
import { User, TodoItem } from 'src/app/shared/models/models';
import { TodosService } from 'src/app/shared/services/todos.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'todos-list',
  templateUrl: './todos-list.component.html',
  styleUrls: ['./todos-list.component.css']
})
export class TodosListComponent implements OnInit {

  users: Object;
  todosList: TodoItem[];
  currentUser: User;

  constructor(private userService: UserService, private todosService: TodosService, private router: Router) { }

  ngOnInit() {
    this.users = JSON.parse(sessionStorage.getItem('UsersAvailable'));
    this.todosList = this.todosService.getTodos();
    this.currentUser = this.userService.getCurrentUser();
  }

  createUpdateItem(index?: number) {
    if (index || index === 0) {
      this.router.navigate(['todoslist/', index]);
    } else {
      this.router.navigateByUrl('todoslist/' + 'new');
    }
  }

  deleteItem(index: number) {
    this.todosList = this.todosService.deleteTodoItem(index);
  }

  checkUserCanCreate(): Boolean {
    return this.currentUser.privlage._CanCreate;
  }

  checkUserCanEdit(item): Boolean {
    if (this.currentUser.privlage._CanEdit && this.currentUser.privlage._CanEditAll) {
      return true;
    } else if (this.currentUser.privlage._CanEdit && item.createdById === this.currentUser.id) {
      return true;
    } else {
      return false;
    }
  }

  checkUserCanDelete(item): Boolean {
    if (this.currentUser.privlage._CanDelete && this.currentUser.privlage._CanDeleteAll) {
      return true;
    } else if (this.currentUser.privlage._CanDelete && item.createdById === this.currentUser.id) {
      return true;
    } else {
      return false;
    }
  }

}
