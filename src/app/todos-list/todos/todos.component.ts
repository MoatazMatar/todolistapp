import { Component, OnInit } from '@angular/core';

import { TodosService } from 'src/app/shared/services/todos.service';
import { UserService } from 'src/app/shared/services/user.service';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {

  constructor(private userService: UserService, private todosService: TodosService) { }

  ngOnInit() {
  }

  logout() {
    this.todosService.resetTodos();
    this.userService.logOut();
  }

}
