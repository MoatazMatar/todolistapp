import { Component } from '@angular/core';

import { UserService } from './shared/services/user.service';
import { User } from './shared/models/models';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private userService: UserService) {
    this.userService.getUsers();
  }

}
