import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { UserService } from 'src/app/shared/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './app-login.component.html',
  styleUrls: ['./app-login.component.css']
})
export class AppLoginComponent implements OnInit {

  LoginForm: FormGroup;
  loginErorr: Boolean = false;

  constructor(private fb: FormBuilder, private userService: UserService, private router: Router) { }

  ngOnInit() {
    this.LoginForm = this.fb.group({
      userName: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }

  login() {
    this.loginErorr = false;
    const loginFormData = this.LoginForm.value;
    if (this.userService.login(loginFormData.userName, loginFormData.password)) {
      sessionStorage.setItem('logged', 'true');
      this.router.navigate(['todoslist']);
    } else {
      this.loginErorr = true;
    }
  }

}
