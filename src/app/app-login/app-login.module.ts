import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppLoginRoutingModule } from './app-login-routing.module';
import { AppLoginComponent } from './app-login-component/app-login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [AppLoginComponent],
  imports: [
    CommonModule,
    AppLoginRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class AppLoginModule { }
