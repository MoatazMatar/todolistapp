export class TodoItem {
    id: number;
    title: string;
    description: string;
    complete: Boolean;
    createdById: number;
}

export class User {
    id: number;
    name: string;
    userName: string;
    password: string;
    privlage: UserPrivlage;
}

export class UserPrivlage {
    _CanView: Boolean = true;
    _CanEdit: Boolean;
    _CanCreate: Boolean;
    _CanDelete: Boolean;
    _CanEditAll: Boolean;
    _CanDeleteAll: Boolean;

    constructor (CanEdit: Boolean, CanCreate: Boolean, CanDelete: Boolean, CanEditAll: Boolean, CanDeleteAll: Boolean) {
        this._CanEdit = CanEdit || false;
        this._CanCreate = CanCreate || false;
        this._CanDelete = CanDelete || false;
        this._CanEditAll = CanEditAll || false;
        this._CanDeleteAll = CanDeleteAll || false;
    }
}
