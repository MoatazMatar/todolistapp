import { Injectable } from '@angular/core';

import todosList from './../data/todos-list.json';
import { TodoItem } from '../models/models.js';

@Injectable({
  providedIn: 'root'
})
export class TodosService {

  todos: TodoItem[] = todosList;

  constructor() {
    this.updateTodos();
  }

  resetTodos(): void {
    this.todos = todosList;
    sessionStorage.setItem('todos', JSON.stringify(this.todos));
  }

  updateTodos() {
    if (sessionStorage.getItem('todos')) {
      const storedTodos = JSON.parse(sessionStorage.getItem('todos'));
      this.todos = [];
      Object.keys(storedTodos).forEach((key) => {
        this.todos.push(storedTodos[key]);
      });
    }
    sessionStorage.setItem('todos', JSON.stringify(this.todos));
  }

  getTodos(): TodoItem[] {
    return this.todos;
  }

  deleteTodoItem(index: number): TodoItem[] {
    this.todos.splice(index, 1);
    sessionStorage.setItem('todos', JSON.stringify(this.todos));
    this.updateTodos();
    return this.todos;
  }

  updateItem(index: number, item: TodoItem): TodoItem[] {
    this.todos[index] = item;
    sessionStorage.setItem('todos', JSON.stringify(this.todos));
    this.updateTodos();
    return this.todos;
  }

  createItem(item: TodoItem): TodoItem[] {
    this.todos[this.todos.length] = item;
    sessionStorage.setItem('todos', JSON.stringify(this.todos));
    this.updateTodos();
    return this.todos;
  }

  getTodoByIndex(index: number): TodoItem {
    return this.todos[index];
  }

  getNewTodoId(): number {
    return this.todos.length;
  }

}
