import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { User, UserPrivlage } from '../models/models';
import users from '../data/users.json';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  users: User[] = users;
  usersMap: Object = {};
  currentUser: User;

  constructor(private router: Router) { }

  public getUsers(): void {
    const newUsers: User[] = [];
    users.forEach((user: User) => {
      const newUser = {
        id: user.id,
        name: user.name,
        userName: user.userName,
        password: user.password,
        // tslint:disable-next-line:max-line-length
        privlage: new UserPrivlage(user.privlage['CanEdit'], user.privlage['CanCreate'], user.privlage['CanDelete'], user.privlage['CanEditAll'], user.privlage['CanDeleteAll'])
      };
      this.usersMap[user.userName] = newUser;
      newUsers.push(newUser);
    });
    sessionStorage.setItem('UsersAvailable', JSON.stringify(this.usersMap));
  }

  public login(userName: String, password: String): Boolean {
    if (this.usersMap[userName.trim().toLowerCase()]) {
      const selectedUser = this.usersMap[userName.trim().toLowerCase()];
      if (selectedUser.password === password) {
        this.currentUser = selectedUser;
        sessionStorage.setItem('currentUser', JSON.stringify(this.currentUser));
        return true;
      } else { return false; }
    } else { return false; }
  }

  public getCurrentUser(): User {
    this.currentUser = JSON.parse(sessionStorage.getItem('currentUser'));
    return this.currentUser;
  }

  public logOut(): void {
    sessionStorage.clear();
    this.router.navigate(['login']);
  }
}
